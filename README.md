# 4chan Emote

Script to bring back 4chan emotes from April Fool's 2022

## Installing

Install a userscript browser addon like Tampermonkey or Greasemonkey

Download emote.user.js, you should be prompted to add the script automatically.

## Using

Type the emote names between two colons and anyone else who has the addon can see them. :POGGERS:

The emotes are case sensitive (only because REEeee and REEEEE are two different emotes)

For a list of valid ones, look at emote.js

## Things to do

- More efficient replacement algorithm?

- UI?

